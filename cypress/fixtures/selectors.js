export const mapper = {
    popUpBoxNo:".at-cm-no-button",
    popUpBoxYes:".at-cm-yes-button",
    checkDemo:".open > .dropdown-menu > :nth-child(2) > a",
    singleCheckBox:"#isAgeSelected",
    checkAll:"#check1",
    inputFromDropDown:":nth-child(1) > :nth-child(1) > .dropdown-toggle",
    radioButtonDemo:".open > .dropdown-menu > :nth-child(3) > a",
    alertAndModal:".navbar-right > :nth-child(2) > .dropdown-toggle",
    bootstrapAlert:".open > .dropdown-menu > :nth-child(1) > a",
    listBox:".navbar-right > :nth-child(3) > .dropdown-toggle",
    bootstrapListBox:".open > .dropdown-menu > :nth-child(1) > a",

    maleRadioBtn:".panel-body > :nth-child(2) > input",
    getCheckedValueBtn:"#buttoncheck",
    getCheckedValueText:".radiobutton",
    groupRadioBtnFemale:".panel-body > :nth-child(2) > :nth-child(3) > input",
    ageGroup5to15:":nth-child(3) > :nth-child(3) > input",
    ageGroupGetValueBtn:".panel-body > .btn",
    ageGroupgetValueText:".groupradiobutton",

    autoCloseSuccBtn:"#autoclosable-btn-success",
    normalSuccMsg:"#normal-btn-success",
    alertNomral:".alert-normal-success",

    searchBox:".list-left > .well > #listhead > .col-md-10 > .input-group > .form-control",
    bootStrapDaulList:".list-left > .well > .list-group > :nth-child(1)",
    moveRightBtn:".move-right > .glyphicon",
    activeRightList:".active",
    bootstrapModal:".open > .dropdown-menu > :nth-child(2) > a",
    launchModal:":nth-child(2) > .col-md-4 > .panel > .panel-body > [data-toggle='modal']",
    modalMessage:"#myModal0 > .modal-dialog > .modal-content > .modal-body"
}
