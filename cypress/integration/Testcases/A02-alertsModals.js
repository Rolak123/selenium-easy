import {mapper} from "../../fixtures/selectors.js";

describe("Given I am on the Alert and Modals page", function () {
    beforeEach(function () {
        cy.visit('/')
    });

    it("Bootstrap Modal  - I should be able to check one of the alerts and modals", function () {
      cy.get(mapper.popUpBoxNo).click()
      cy.get(mapper.alertAndModal).click()
      cy.get(mapper.bootstrapModal).click()
      cy.get(mapper.launchModal).click()
      cy.get(mapper.modalMessage).should('include.text', "This is the place where the content for the modal dialog displays");
    })

    it("Bootstrap Alert  - I should be able to check one of the alerts and modals", function () {
      cy.get(mapper.popUpBoxNo).click()
      cy.get(mapper.alertAndModal).click()
      cy.get(mapper.bootstrapAlert).click()
      cy.get(mapper.normalSuccMsg).click()
      cy.get(mapper.alertNomral).should('include.text', "I'm a normal success message.");
    })

})