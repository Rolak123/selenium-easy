import {mapper} from "../../fixtures/selectors.js";

describe("Given I am on the input forms page", function () {
    beforeEach(function () {
        cy.visit('/')
    });

    it("Check Demo - I should be able to check one of the input forms", function () {
      cy.get(mapper.popUpBoxNo).click()
      cy.get(mapper.inputFromDropDown).click()
      cy.get(mapper.checkDemo).click()
      cy.get(mapper.singleCheckBox).click()
      cy.get(mapper.checkAll).click()
    })
    
    it("Radio Button - I should be able to check one of the input forms", function () {
      cy.get(mapper.popUpBoxNo).click()
      cy.get(mapper.inputFromDropDown).click()
      cy.get(mapper.radioButtonDemo).click()
      cy.get(mapper.maleRadioBtn).click()
      cy.get(mapper.getCheckedValueBtn).click()
      cy.get(mapper.getCheckedValueText).should('be.visible').should('contain', "Radio button 'Male' is checked");
      cy.get(mapper.groupRadioBtnFemale).click()
      cy.get(mapper.ageGroup5to15).click()
      cy.get(mapper.ageGroupGetValueBtn).click()
      cy.get(mapper.ageGroupgetValueText).should('be.visible').should('contain', "Sex : Female");
      cy.get(mapper.ageGroupgetValueText).should('be.visible').should('contain', " Age group: 5 - 15");
    })




})