import {mapper} from "../../fixtures/selectors.js";

describe("Given I am on the list form page", function () {
    beforeEach(function () {
        cy.visit('/')
    });

    it("BoostrapListBox - I should be able to check one of the alerts and modals", function () {
      cy.get(mapper.popUpBoxNo).click()
      cy.get(mapper.listBox).click()
      cy.get(mapper.bootstrapListBox).click()
      cy.get(mapper.searchBox).type('boot')
      cy.get(mapper.bootStrapDaulList).click()
      cy.get(mapper.moveRightBtn).click()
      cy.get(mapper.activeRightList).should('include.text', "bootstrap-duallist ");
    })

})