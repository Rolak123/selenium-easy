# Cypress Automation
Quidax Skill base Test using cyypress Mocha 

#  Framework u

1. Framework: Cypress
2. Supporting Language: Javascript

# Tools
3. Visual Studio Code (Editor)
4. Node.js (12 or Higher)
5. Mocha

# Supported Browsers
The following are the supported browsers as of this version; 7.6.0
Chrome
Edge
Electron (default)
Firefox



## Getting Started
```
1. git clone https://gitlab.com/Rolak123/selenium-easy/-/tree/master
2. Open the project on VS Code
3. Open the terminal, Perform "npm i" to install all the dependencies present in the package.json file.
```

## Runing a Test
```
1. In the terminal, perform `npx cypress run` to start the script execution
```

### Reports used
- Mochawesome Html reporter
- Path: Report can be found in cypress/reports/index.htm


